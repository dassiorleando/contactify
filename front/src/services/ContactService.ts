import axios from 'axios';
import { ContactModel} from '../components/contact/Model';

// Root API endpoint
// TODO: To be set in an env var later at build time
const API_ENDPOINT = 'http://localhost:5000/api/contacts';

/**
 * Contact service
 * Containing specific contacts related functions and/or API requests
 * @author dassiorleando
 */
const ContactService = {

  /**
   * Saves a contact
   * In case the id is provided it'll perform a PUT operation
   * Else a POST is done
   * @param contact The contact details to save
   * @returns A promise that resolves to the saved contact
   */
  save: function (contact: ContactModel): Promise<ContactModel> {
    return new Promise(async (resolve, reject) => {
      if (!contact || !contact.firstName || !contact.lastName || !contact.phone || !contact.gender) return reject('Bad entries, please fill the form properly');
      try {
        let httpResponse: any = {};
        if (contact._id) {
          httpResponse = await axios.put(API_ENDPOINT, contact);
        } else {
          httpResponse = await axios.post(API_ENDPOINT, contact);
        }
        const response: any = httpResponse.data;
        response.success ? resolve(response.data || {}) : reject(response.message);
      } catch (error) {
        let message = error.response && error.response.data && error.response.data.message;
        reject(message);
      }
    });
  },
  
  /**
   * Loads the contacts list
   * TODO: paginating the results server-side to make it more scalable
   * @returns A promise that resolves to the contacts list
   */
  getContacts: function (): Promise<ContactModel> {
    return new Promise(async (resolve, reject) => {
      try {
        const { data } = await axios.get(API_ENDPOINT);
        const response: any = data;
        response.success ? resolve(response.data || []) : reject(response.message);
      } catch (error) {
        let message = error.response && error.response.data && error.response.data.message;
        reject(message);
      }
    });
  },

  /**
   * Finds a specific contact by its id
   * @param contactId The targetted contact's id
   * @returns A promise that resolves to the contact found
   */
  findById: function (contactId: string): Promise<ContactModel> {
    return new Promise(async (resolve, reject) => {
      if (!contactId) return reject('Bad contact id provided');
      try {
        const { data } = await axios.get(`${API_ENDPOINT}/${contactId}`);
        const response: any = data;
        response.success ? resolve(response.data || {}) : reject(response.message);
      } catch (error) {
        let message = error.response && error.response.data && error.response.data.message;
        reject(message);
      }
    });
  },

  /**
   * Deletes a specific contact by its id
   * @param contactId The targetted contact's id
   * @returns A promise that resolves to a boolean
   */
  delete: function (contactId: string): Promise<boolean> {
    return new Promise(async (resolve, reject) => {
      if (!contactId) return reject('Bad contact id provided');
      try {
        const { data } = await axios.delete(`${API_ENDPOINT}/${contactId}`);
        const response: any = data;
        response.success ? resolve(true) : reject(response.message);
      } catch (error) {
        let message = error.response && error.response.data && error.response.data.message;
        reject(message);
      }
    });
  },

  /**
   * Gets the contact statistics
   * @returns A promise that resolves to the contacts statistics
   */
  getStatistics: function (): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        const { data } = await axios.get(`${API_ENDPOINT}/stats`);
        const response: any = data;
        response.success ? resolve(response.data || {}) : reject(response.message);
      } catch (error) {
        let message = error.response && error.response.data && error.response.data.message;
        reject(message);
      }
    });
  }

}

export default ContactService;
