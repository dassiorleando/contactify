import { toast } from 'react-toastify';
import { ContactModel } from '../components/contact/Model';

/**
 * Helper functions
 * @author dassiorleando
 */
const defaultToastOptions: any = {
  position: "top-right",
  autoClose: 5000,
  hideProgressBar: true,
  closeOnClick: true,
  pauseOnHover: true,
  draggable: true,
  progress: undefined,
}

const Helper = {

  toastInfo: function (message: string, options = defaultToastOptions) {
    toast.info(message, options);
  },
  
  toastSuccess: function (message: string, options = defaultToastOptions) {
    toast.success(message, options);
  },
  
  toastError: function (message: string, options = defaultToastOptions) {
    toast.error(message, options);
  },

  toastWarning: function (message: string, options = defaultToastOptions) {
    toast.warning(message, options);
  },

  /**
   * Set element into input fields
   * @param element The dom element to set the value in
   * @param value The actual value to use
   * @returns void
   */
  setNativeValue: function (element: any, value: any) {
    if (element) {
      let lastValue = element.value;
      element.value = value;
      let event = new Event("input", { bubbles: true });
      let tracker = element._valueTracker;
      if (tracker) {
          tracker.setValue(lastValue);
      }
      element.dispatchEvent(event);
    }
  },

  fakeContacts(): ContactModel[] {
    return [
      {
        _id: '1',
        firstName: 'Fake first name 1',
        lastName: 'Fake last name 1',
        phone: 'Fake phone 1',
        gender: 'MALE',
      },
      {
        _id: '2',
        firstName: 'Fake first name 2',
        lastName: 'Fake last name 2',
        phone: 'Fake phone 2',
        gender: 'FEMALE',
      },
      {
        _id: '3',
        firstName: 'Fake first name 3',
        lastName: 'Fake last name 3',
        phone: 'Fake phone 3',
        gender: 'MALE',
      }
    ];
  }

}

export default Helper;
