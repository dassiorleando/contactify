import React from 'react';
import App from '../components/app';
import Graph from '../components/graph';
import Contact from '../components/contact';
import Contacts from '../components/contacts';
import MainLayout from '../components/mainLayout';
import EditContact from '../components/editContact';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

/**
 * Main route file
 * It lists all the routes here (dynamic & static)
 * @author dassiorleando
 */
const MainRoutes = () => {
  return (
    <MainLayout>
      <Router>
        <Switch>
          <Route exact path="/" component={App}/>
          <Route path="/landing" component={Graph}/>
          <Route path="/contacts" component={Contacts}/>
          <Route exact path="/contact/new" component={EditContact}/>
          <Route exact path="/contact/:contactId/edit" component={EditContact}/>
          <Route path="/contact/:contactId" component={Contact}/>

          <Route path="*">
            <div>PAGE NOT FOUND</div>
          </Route>
        </Switch>
      </Router>
    </MainLayout>
  );
};

export default MainRoutes;
