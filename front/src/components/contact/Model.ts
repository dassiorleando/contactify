/**
 * Contact model
 * @author dassiorleando
 */
export class ContactModel {
  constructor(
    public _id?: string,
    public firstName?: string,
    public lastName?: string,
    public phone?: string,
    public gender?: string) {}
}
