import React from 'react';
import './Contact.scss';
import Helper from '../../services/Helper';
import { ContactModel } from './Model';
import { Button, Col, ListGroup, ListGroupItem, Row, Spinner } from 'reactstrap';
import ContactService from '../../services/ContactService';
import { Link } from 'react-router-dom';

/**
 * Component that shows a contact's details
 * @author dassiorleando
 */
class Contact extends React.Component<any, any> {
  loading = true;
  contactId: string;             // The contact id
  stateContact: ContactModel;    // Contact details coming from the location state

  constructor (props: any) {
    super(props);
    this.state = { contact: {} };
    this.stateContact = (this.props.location.state && this.props.location.state.contact) || {};

    // URL parameter: contactId
    this.contactId = this.props.match.params.contactId;

    // Allowing 'this' to work in the event handlers
    this.deleteContact = this.deleteContact.bind(this);
  }

  /**
   * In case the location state isn't enough we can do an API request to get extensive contact details
   */
  async componentDidMount() {
    try {
      const contact = await ContactService.findById(this.contactId);
      this.loading = false;
      this.setState({ contact });
      this.setPageTitle();
    } catch (message) {
      this.loading = false;
      // In case of any errors we use the state data if present, unless we toast the API error message
      if (!this.stateContact || !this.stateContact._id) {
        Helper.toastError(message || 'An error occured while getting the contact');
      } else {
        this.setState({ contact: this.stateContact });
        this.setPageTitle();
      }
    }
  }

  /**
   * Sets the page title with the contact first and last names
   */
  setPageTitle(): void {
    const contact = this.state.contact;
    if (contact && contact._id) document.title = `Contact - ${contact.firstName}:${contact.lastName}`;
  }

  /**
   * Deletes a contact by its id
   * It redirects to the contacts listing page once successfully deleted
   */
  async deleteContact() {
    if (window.confirm('Do you really want to delete this contact?')) { 
      try {
        const deleted = await ContactService.delete(this.contactId);
        if (deleted) {
          Helper.toastSuccess('Contact deleted successfully');
          this.props.history.push('/contacts');
        }
      } catch (message) {
        Helper.toastError(message || 'An error occured while deleting the contact');
      }
    }
  }

  render() {
    const { contact } = this.state;

    return (
      <div className="contact-details">
        <h2 className="text-primary text-center"> Contact Details </h2>
        <br/>

        {/* Shows the spinner/loader if the AJAX request is ongoing */}
        { this.loading
            ? <Spinner animation="border" color="primary" className="contactify-spinner center-block mt-50" />
            : <ListGroup>
              <ListGroupItem>
                <Row>
                  <Col sm={6} xs={12} className="text-center font-weight-bold"> First Name </Col>
                  <Col sm={6} xs={12} className="text-center text-capitalize"> {contact.firstName} </Col>
                </Row>
              </ListGroupItem>
              <ListGroupItem>
                <Row>
                  <Col sm={6} xs={12} className="text-center font-weight-bold"> Last Name </Col>
                  <Col sm={6} xs={12} className="text-center text-capitalize"> {contact.lastName} </Col>
                </Row>
              </ListGroupItem>
              <ListGroupItem>
                <Row>
                  <Col sm={6} xs={12} className="text-center font-weight-bold"> Phone </Col>
                  <Col sm={6} xs={12} className="text-center"> {contact.phone} </Col>
                </Row>
              </ListGroupItem>
              <ListGroupItem>
                <Row>
                  <Col sm={6} xs={12} className="text-center font-weight-bold"> Gender </Col>
                  <Col sm={6} xs={12} className="text-center"> {contact.gender} </Col>
                </Row>
              </ListGroupItem>
            </ListGroup>
        }

        <br/>

        <Row>
          <Col className="text-center">
            <Button color="danger" className="center-block" onClick={this.deleteContact}>
              <span className="d-none d-sm-inline">Delete</span> <i className="fa fa-remove fs-5d0 text-white text-center"></i>
            </Button>
          </Col>
          <Col className="text-center">
            <Link to={{ pathname: `/contact/${contact._id}/edit`, state: { contact: contact } }}>
              <Button color="primary" className="center-block">
                <span className="d-none d-sm-inline">EDIT</span> <i className="fa fa-pencil"></i>
              </Button>
            </Link>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Contact;
