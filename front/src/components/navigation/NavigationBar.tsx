import React, { useState } from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  NavbarText
} from 'reactstrap';

/**
 * The navigation bar
 * @param props 
 * @author dassiorleando
 */
const NavigationBar = (props: any) => {
  const [isOpen, setIsOpen] = useState(false);

  // The toggler for small screen display of the nav menu
  const toggle = () => setIsOpen(!isOpen);

  return (
    <div>
      <Navbar color="light" light expand="md">
        <NavbarBrand href="/" className="text-primary">Contactify</NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
            <NavItem>
              <NavLink href="/contacts">Contacts</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="/landing">Gender Graph</NavLink>
            </NavItem>
          </Nav>
          <NavbarText>
            <i className="fa fa-info"></i>
          </NavbarText>
        </Collapse>
      </Navbar>
    </div>
  );
}

export default NavigationBar;
