import React from 'react';
import './EditContact.scss';
import Helper from '../../services/Helper';
import ContactService from '../../services/ContactService';
import { Button, Col, Form, FormGroup, Input, Label, Row, Spinner } from 'reactstrap';

/**
 * Edit contact component
 * Allowing to create/edit a contact
 * @author dassiorleando
 */
class EditContact extends React.Component<any, any> {
  formData: any;          // The form data to send to the API to create the contract
  contactId: string;      // The contact id if sent for edit purpose

  constructor (props: any) {
    super(props);
    this.formData = this.formData || {};
    
    // URL parameter: contactId
    this.contactId = this.props.match.params.contactId;
    this.state = { contactToEdit: {} };

    // Allowing 'this' to work in the event handlers
    this.handleChange = this.handleChange.bind(this);
    this.saveContact = this.saveContact.bind(this);
  }

  /**
   * Once the component is mounted we set the contact to edit if provided via location state
   */
  async componentDidMount() {
    document.title =  'New Contact';
    let contactToEdit = (this.props.location.state && this.props.location.state.contact) || {};

    // If the contact isn't provided into the location state we fetch it using its id if present
    if (!contactToEdit._id && this.contactId) {
      this.setState({ loading: true });
      try {
        contactToEdit = await ContactService.findById(this.contactId);
        this.setState({ loading: false });
      } catch (message) {
        this.setState({ loading: false });
        Helper.toastError(message || 'An error occured while getting the contact to update');
      }
    }

    // Setting the values
    this.formData = contactToEdit;
    this.setContactInForm();
  }

  /**
   * Set the contact to edit into the form
   */
  setContactInForm(): void {
    // Looping over the contact fields to set the form inputs
    if (this.formData) {
      this.setState({ contactToEdit: this.formData });
      document.title = `Update contact - ${this.formData.firstName}:${this.formData.lastName}`;

      for (var field in this.formData) {
        if (Object.prototype.hasOwnProperty.call(this.formData, field)) {
          var input = document.getElementById(field);
          Helper.setNativeValue(input, this.formData[field]);
        }
      }
    }
  }

  /**
   * Handling the form input changes
   * To build the contact details (form data)
   * @param e 
   */
  handleChange(e: any): void {
    this.formData[e.target.id] = e.target.value;
  }

  /**
   * Saves the contact
   * It redirects to the contacts listing page once successfully created
   */
  async saveContact(e: any) {
    e.preventDefault();
    this.setState({ loading: true });

    // Default value for the gender
    this.formData.gender = this.formData.gender || 'MALE';
    
    try {
      await ContactService.save(this.formData);
      this.setState({ loading: false });
      Helper.toastSuccess('Contact saved successfully.');

      // We redirect if it's a create action
      if (!this.state.contactToEdit._id) this.props.history.push('/contacts');
    } catch (message) {
      this.setState({ loading: false });
      Helper.toastError(message || 'An error occured while loading the contact.');
    }

    // Keeping the form populated if it's an update
    if (this.state.contactToEdit._id) this.setContactInForm();
  }
  
  render() {
    return (
      <div className="new-contact">
        <h2 className="text-primary text-center"> { this.state.contactToEdit._id ? <span>Edit</span> : <span>New</span> } Contact </h2>

        {/* Shows the spinner/loader if the AJAX request is ongoing */}
        { this.state.loading
            ? <Spinner animation="border" color="primary" className="contactify-spinner center-block mt-50" />
            : <Form>
                <FormGroup>
                  <Label for="firstName">First Name</Label>
                  <Input type="text" name="firstName" id="firstName" placeholder="First Name" onChange={this.handleChange} />
                </FormGroup>
                <FormGroup>
                  <Label for="lastName">Last Name</Label>
                  <Input type="text" name="lastName" id="lastName" placeholder="Last Name" onChange={this.handleChange} />
                </FormGroup>
                <FormGroup>
                  <Label for="phone">Phone (+country code)</Label>
                  <Input type="text" name="phone" id="phone" placeholder="Phone Number" onChange={this.handleChange} />
                </FormGroup>
                <FormGroup>
                  <Label for="gender">Gender</Label>
                  <Input type="select" name="select" id="gender" onChange={this.handleChange}>
                    <option>MALE</option>
                    <option>FEMALE</option>
                  </Input>
                </FormGroup>

                <Row>
                  <Col className="text-center">
                    <Button color="danger" className="center-block" onClick={() => { this.props.history.push('/contacts') } }>
                      <span className="d-none d-sm-inline">Back</span> <i className="fa fa-backward"></i>
                    </Button>
                  </Col>
                  <Col className="text-center">
                    <Button color="primary" className="center-block" onClick={this.saveContact}>
                      <span className="d-none d-sm-inline">SAVE</span> <i className="fa fa-save"></i>
                    </Button>
                  </Col>
                </Row>
              </Form>
          }
      </div>
    );
  }

}

export default EditContact;
