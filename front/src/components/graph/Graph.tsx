import React from 'react';
import './Graph.scss';
import Helper from '../../services/Helper';
import ReactTooltip from 'react-tooltip';
import { Col, Row, Spinner } from 'reactstrap';
import { PieChart } from 'react-minimal-pie-chart';
import ContactService from '../../services/ContactService';

/**
 * Graph component
 * Shows the gender distribution in pie charts
 * @author dassiorleando
 */
class Graph extends React.Component<any, any> {
  stats: any = {};    // Overall statistics data
  defaultStats = {    // Default stats to use in case the server is not running
    valid: 15,
    deleted: 5,
    males: 5,
    females: 10,
    total: 20,
    fake: true
  };

  constructor (props: any) {
    super(props);
    this.state = {
      loading: true
    }
  }

  /**
   * Once the component is mounted we get the contacts statistics from the API/DB
   */
  async componentDidMount() {
    document.title = 'Gender Graph';
    try {
      this.stats = await ContactService.getStatistics();
      this.setState({ loading: false });
    } catch (message) {
      if (!this.props.asSection) Helper.toastError(message || 'An error occured while getting the contacts statistics');
      this.stats = this.defaultStats;
      this.setState({ loading: false });
    }
  }

  /**
   * Returns the right tooltip message depending on the mouse click/hover actions on the charts
   * @param index The index of the targetted chart element
   * @returns The text to show in the tooltip
   */
  buildTooltipMessage(index: number) {
    if (index === 100) {
      return `${this.stats.valid} valid males/females contact${ this.stats.valid > 1 ? 's' : ''} for ${this.stats.deleted}/${this.stats.total} deleted ${this.stats.fake ? '(fake)' : ''}`;
    } else if (index === 0) {
      return `Male ${this.stats.fake ? '(fake)' : ''} ${this.stats.males} `;
    } else {
      return `Female ${this.stats.fake ? '(fake)' : ''} ${this.stats.females} `;
    }
  }

  render() {
    const pieLabelsStyle = {
      fontSize: '5px',
      fontFamily: 'sans-serif',
      fill: '#552a7b',
      opacity: 0.5,
    };

    return (
      <div className="gender-graph">
        {/* Hide the title if the component is rendering as a section (not in a full page) */}
        {
          !this.props.asSection && <h2 className="text-primary text-center"> Gender Graph </h2>
        }

        {/* Shows the spinner/loader if the AJAX request is ongoing */}
        { this.state.loading
          ? <Spinner animation="border" color="primary" className="contactify-spinner center-block mt-50" />
          : <Row data-tip="" data-for="chart">
              <Col sm={6} xs={12}>
                <PieChart
                  animate
                  radius={25}
                  lineWidth={60}
                  segmentsShift={(index) => (index === 0 ? 2 : 0.5)}
                  segmentsStyle={{ transition: 'stroke .3s', cursor: 'pointer' }}
                  data={[
                    { title: 'MALES', value: this.stats.males, color: '#899c83' },
                    { title: 'FEMALES', value: this.stats.females, color: '#97839c' }
                  ]}
                  label={({ dataEntry }) => Math.round(dataEntry.percentage) + '%'}
                  labelPosition={70}
                  labelStyle={{
                    ...pieLabelsStyle,
                  }}
                  onMouseOver={(_, index) => {
                    this.setState({ hovered: index })
                  }}
                  onMouseOut={() => {
                    this.setState({ hovered: null })
                  }}
                />
              </Col>

              <Col sm={6} xs={12}>
                <PieChart
                  animate
                  radius={25}
                  lineWidth={20}
                  data={[{ value: this.stats.valid, color: '#c3b2b7a6' }]}
                  totalValue={this.stats.total}
                  label={({ dataEntry }) => dataEntry.value}
                  labelStyle={{
                    fontSize: '25px',
                    fontFamily: 'sans-serif',
                    fill: '#552a7b',
                  }}
                  labelPosition={0}
                  onMouseOver={(_, index) => {
                    this.setState({ hovered: 100 })
                  }}
                  onMouseOut={() => {
                    this.setState({ hovered: null })
                  }}
                />
              </Col>

              {/* The tooltip that renders the chart labels on mouse click/hover events */}
              <ReactTooltip
                id="chart"
                getContent={() =>
                  typeof this.state.hovered === 'number' ? this.buildTooltipMessage(this.state.hovered) : null
                }
              />
            </Row>
        }
      </div>
    );
  }

}

export default Graph;
