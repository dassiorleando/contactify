import React from 'react';
import logo from './logo.svg';
import './App.scss';
import { Button, Container } from 'reactstrap';
import Graph from '../graph';
import { Link } from 'react-router-dom';

/**
 * The main/home page of the App
 * @author dassiorleando
 */
function App() {
  return (
    <div className="App">
      <section className="App-Content">
        <Container>
          <img src={logo} className="App-logo" alt="logo" />
          <br/>
          <p>
            Contactify helps you to easily manage your contacts
          </p>

          <Graph asSection={true}/>
          
          <Link to={{ pathname: '/contacts'}}>
            <Button color="primary" className="center-block">
              <span className="d-none d-sm-inline">Manage Contacts</span> <i className="fa fa-cog"></i>
            </Button>
          </Link>
        </Container>
      </section>
    </div>
  );
}

export default App;
