import React from 'react';
import './Contacts.scss';
import Helper from '../../services/Helper';
import { Link } from 'react-router-dom';
import { Button, Card, CardBody, Spinner, Table } from 'reactstrap';
import ContactService from '../../services/ContactService';
import { ContactModel } from '../contact/Model';

/**
 * Displaying the list of saved contact
 * @author dassiorleando
 */
class Contacts extends React.Component<any, any> {

  constructor (props: any) {
    super(props);
    this.state = { contacts: [], loading: true };
  }

  /**
   * Once the component is mounted we load the contacts list from the API/DB
   */
  async componentDidMount() {
    document.title = 'Contacts List';
    try {
      const contacts = await ContactService.getContacts();
      this.setState({ contacts, loading: false });
    } catch (message) {
      if (!message) this.setState({ loading: false, printingFakeData: true });
      Helper.toastError(message || 'An error occured while fetching the contacts');
    }
  }

  render() {
    let { contacts } = this.state;

    // In case of empty list we use the fake list for demo/testing purposes
    contacts = !contacts || contacts.length === 0 ? Helper.fakeContacts() : contacts;

      return (
        <div className="contacts">
          <h2 className="text-primary text-center"> Contacts </h2>

          {/* Shows the spinner/loader if the AJAX request is ongoing */}
          { this.state.loading
            ? !this.state.printingFakeData ? <Spinner animation="border" color="primary" className="contactify-spinner center-block mt-50" /> : ''
            : <Card>
              <CardBody>
                <Table className="table-hover">
                  <thead className="table-primary">
                    <tr>
                      <th>#</th>
                      <th>First Name</th>
                      <th className="d-none d-sm-table-cell"> Last Name </th>
                      <th className="d-none d-sm-table-cell"> Phone </th>
                      <th className="d-none d-sm-table-cell"> Gender </th>
                      <th className="d-sm-none"> </th>
                    </tr>
                  </thead>
                  <tbody>
                    {contacts.map((contact: ContactModel, index: number) => {
                      return (
                        <tr key={contact._id}>
                          <th scope="row"> <Link to={{ pathname: `/contact/${contact._id}`, state: { contact: contact } }}> {index + 1} </Link></th>
                          <td>{contact.firstName}</td>
                          <td className="d-none d-sm-table-cell"> {contact.lastName} </td>
                          <td className="d-none d-sm-table-cell"> {contact.phone} </td>
                          <td className="d-none d-sm-table-cell"> {contact.gender} </td>
                          <td className="d-sm-none"> <Link to={{ pathname: `/contact/${contact._id}`, state: { contact: contact } }}> <i className="fa fa-eye"></i> </Link> </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </Table>

                {/* New contact action button */}
                <Link to={{ pathname: '/contact/new'}}>
                  <Button color="primary" className="center-block">
                    <span className="d-none d-sm-inline">New Contact</span> <i className="fa fa-plus"></i>
                  </Button>
                </Link>
              </CardBody>
            </Card>
          }
        </div>
      );
  }
}

export default Contacts;
