import React from 'react';
import './MainLayout.scss';
import { Container } from 'reactstrap';
import { ToastContainer } from 'react-toastify';
import NavigationBar from '../navigation';

/**
 * The main layout used for all the routes/pages/sections
 * It describes the overall structure of the layout: navigation and container
 * @author dassiorleando
 */
function MainLayout({ children }: any) {
  return (
    <div id="app-layout">
      <ToastContainer/>
      
      <header>
        <NavigationBar />
      </header>

      <Container className="p-20">{children}</Container>
    </div>
  );
}

export default MainLayout;
