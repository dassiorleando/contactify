export NODE_ENV=prod
export PORT=5000

echo "Pulling the most updated version"
git pull

echo "Installing missed dependencies"
npm i

echo "Kill the former process/deployment"
forever stop app.local.js
# killall -s KILL node
# sudo kill $(sudo lsof -t -i:5000)

echo "Run the project"
# PORT=5000 && nohup node app.local.js &
forever start app.local.js
