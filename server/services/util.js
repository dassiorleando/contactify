/**
 * Some useful utilities
 * @author dassiorleando
 */

/**
 * Default callback doing nothing
 * @returns {void}
 */
exports.noop = function () {
}

/**
 * Prints error
 * @param {Object} error The error to show
 * @returns {void}
 */
exports.error = function (error) {
  console.error('❌ Something went wrong', error);
}

/**
 * Gets the error message from error object
 * @param {Object} err The error to get the error message from
 * @returns {string} The message to send back to the user who performed a request/action
 */
const getUniqueErrorMessage = function (err) {
  let output;

  try {
    const fieldName = err.errmsg.substring(err.errmsg.lastIndexOf('.$') + 2, err.errmsg.lastIndexOf('_1'));
    output = fieldName.charAt(0).toUpperCase() + fieldName.slice(1) + ' already exists';
  } catch (ex) {
    output = 'Unique field already exists';
  }

  return output;
};

/**
 * Gets the error message from error object
 * @param {Object} err The error to get the error message from
 * @returns {string} The message to send back to the user who performed a request/action
 */
exports.getErrorMessage = function (err) {
  let message = '';

  if (typeof err === 'string') {
    message = err;
  } else if (err.code) {
    switch (err.code) {
      case 11000:
      case 11001:
        message = getUniqueErrorMessage(err);
        break;
      case 'LIMIT_FILE_SIZE': // multer error on file size
        message = err.message;
        break;
      default:
        message = 'Something went wrong';
    }
  } else {
    for (let errName in err.errors) {
      if (err.errors[errName].message) {
        message = err.errors[errName].message;
      }
    }
  }

  return message;
}
