/**
 * Contact service
 * @author dassiorleando
 */
const Contact = require('../models/contact');

/**
 * Creates a contact
 * @param {Object} contact The contact details to create
 * @returns {Promise<Object>} Promise that resolves to the created contact
 */
exports.create = function (contact) {
    if (!contact) return Promise.reject({ success: false, message: 'Bad data provided' });
    return Contact.create(contact);
}

/**
 * Updates a contact
 * @param {string} id The contact's id
 * @param {Object} data The new details to set into the existing contact if found
 * @returns {Promise<Object>} Promise that resolves to the updated contact
 */
exports.update = async function (data) {
    if (!data || !data._id) return Promise.reject({ success: false, message: 'Bad data provided' });

    // Find the right contact
    const contact = await findOne({ _id: data._id, deleted: false });

    if (contact) {
        contact.firstName = data.firstName;
        contact.lastName = data.lastName;
        contact.phone = data.phone;
        contact.gender = data.gender;

        // Explicit save
        return contact.save();
    }

    return Promise.reject({ success: false, status: 404, message: 'Contact not found' });
}

/**
 * Loads all contacts
 * @param {Object} query the query object
 * @param {Object} projection projected fields to add/remove
 * @param {Object} sort sorting criteria
 * @returns {Promise<Object>} Promise that resolves to the contacts list
 */
exports.findAll = function (query = {}, projection = {}, sort = {}) {
    return Contact.find(query, projection, sort);
}

/**
 * Counds all contacts
 * @param {Object} query the query object
 * @returns {Promise<Object>} Promise that resolves to the contacts list
 */
exports.count = function (query = {}) {
    return Contact.count(query);
}

/**
 * Loads one contact
 * @param {Object} query the query object
 * @param {Object} projection projected fields to add/remove
 * @returns {Promise<Object>} Promise that resolves to the contact found
 */
function findOne (query = {}, projection = {}) {
    return Contact.findOne(query, projection);
}
exports.findOne = findOne;
