/**
 * Config file explicitly setting the env vars
 * @author dassiorleando
 */
module.exports = {
    PORT: process.env.PORT || '5000',
    ENV: process.env.NODE_ENV || 'dev',
    MONGODB_URI: process.env.MONGODB_URI || 'mongodb://localhost/rezilio_contactify'
}
