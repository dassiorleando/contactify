/**
 * Setting up contact express app
 * @author dassiorleando
 */
const express = require('express');
const bodyParser = require('body-parser');
const db = require('./lib/db')();
const Util = require('./services/util');
const contactAPI = require('./resources/contact');
const cors = require('cors');
const path = require('path');
const router = express.Router();
const app = express();

// Enable CORS
app.use(cors()); // Enable all CORS requests for all routes

// Parsers
app.use(bodyParser.json({ limit: '50mb' }));                         // Json
app.use(bodyParser.urlencoded({ limit: '50mb', extended: false }));  // form-url-encoded

// Set our api routes
app.use('/', router);
router.use('/api/contacts', contactAPI);

// Middleware for catching unhandled errors
router.use(function (err, req, res, next) {
  Util.error(err);
  res.status(400).json({ success: false, message: Util.getErrorMessage(err) });
});

// Public assets directory
app.use(express.static(path.join(__dirname, '..', 'front', 'build')));
app.use(express.static('public'));

// Serve the index file for other requests
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, '..', 'front', 'build', 'index.html'));
});

module.exports = app;
