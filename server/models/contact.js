/**
 * Contact schema
 * @author dassiorleando
 */
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// create a schema
const contactSchema = new Schema({
  firstName: {
    type: String,
    required: true
  },
  lastName: {
    type: String,
    required: true
  },
  phone: {
    type: String,
    required: true
  },
  gender: {
    type: String,
    default: 'MALE',
    enum: [ 'MALE', 'FEMALE' ]
  },
  deleted: {
    type: Boolean,
    default: false
  },
  created_at: Date,
  updated_at: Date
});

// on every save, add the date and edit updated date
contactSchema.pre('save', function(next) {
  // The current date
  const currentDate = new Date();
  
  // Edit the updated_at field to the current date
  if (!this.keepUpdatedDate) this.updated_at = currentDate;

  // if created_at doesn't exist, add to that field
  if (!this.created_at) this.created_at = currentDate;

  next();
});

// Create a model from the schema
const Contact = mongoose.model('Contact', contactSchema);

// Exports it to be abailable in all the application
module.exports = Contact;
