/**
 * Contact API endpoints
 * @author dassiorleando
 */
const express = require('express');
const router = express.Router();
const status = require('http-status');
const contactService = require('../services/contact');
const mongoose = require('mongoose');
const Util = require('../services/util');

/**
 * API: Creates a new contact
 * 
 * @name post/api/contacts
 * @function
 * @inner
 * @param {string} path - Express path
 * @param {callback} middleware - Express middleware.
 */
router.post('/', async function (req, res) {
  const data = req.body;

  // Contact details validations
  if (!data || !data.firstName || !data.lastName || !data.phone || !data.gender) {
    console.log('Some empty data have been provided');
    return res.status(status.BAD_REQUEST).json({ success: false, message: 'Bad data provided!' });
  }

  // The contact details
  const contactObject = {
    firstName: data.firstName,
    lastName: data.lastName,
    phone: data.phone,
    gender: data.gender,
    deleted: false
  };

  console.log(`Creating the contact for ${contactObject.firstName}`);

  // Explicit save + catching error
  const createdContact = await contactService.create(contactObject).catch(error => {
    console.log('Error while creating the contact');
    res.status(status.BAD_REQUEST).json({ success: false, message: error.message ? error.message : Util.getErrorMessage(error) });
    return;
  });

  if (createdContact && createdContact._id) {
    // Send back the result  
    console.log(`Contact created successfully`);

    return res.status(status.OK).json({ success: true, data: createdContact });
  } else {
    console.log('An error occured while creating the Contact');
  }
});

/**
 * API: Update a contact
 * 
 * @name put/api/contacts
 * @function
 * @inner
 * @param {string} path - Express path
 * @param {callback} middleware - Express middleware.
 */
router.put('/', async (req, res) => {
  const data = req.body;        // Incoming details (new contact details)
  const id = data._id;          // The contact's ID to update

  // Contact's id validation
  if (!id) return res.status(status.BAD_REQUEST).json({ success: false, message: 'Bad id provided' });

  // Check first if it is a valid Id
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(status.BAD_REQUEST).json({ success: false, message: 'Contact id is invalid' });
  }

  console.log(`Updating the contact of first name ${data.firstName}`);

  // Find the contact with id :id
  const updatedContact = await contactService.update(data).catch(error => {
    res.status(error.status || status.BAD_REQUEST).json({ success: false, message: error.message ? error.message : Util.getErrorMessage(error) });
    return;
  });

  if (updatedContact && updatedContact._id) {
    console.log('The contact has been updated');
    return res.status(status.OK).json({ success: true, data: updatedContact });
  } else {
    console.log('Could not update the contact');
  }
});

/**
 * API: gets all saved contacts
 * 
 * @name get/api/contacts
 * @function
 * @inner
 * @param {string} path - Express path
 * @param {callback} middleware - Express middleware.
 */
router.get('/', async (req, res) => {
  const query = {
    deleted: false
  };

  // Sorting by the created dates, most recent on top
  let contacts = await contactService.findAll(query, {}, { created_at: -1 }).catch(Util.error) || [];
  if (!contacts || contacts.length === 0) {
    console.log('No elements to show, we send back the fake contacts');
    contacts = [
      {
        _id: 1,
        firstName: 'Fake first name 1',
        lastName: 'Fake last name 1',
        phone: 'Fake phone 1',
        gender: 'MALE',
      },
      {
        _id: 2,
        firstName: 'Fake first name 2',
        lastName: 'Fake last name 2',
        phone: 'Fake phone 2',
        gender: 'FEMALE',
      },
      {
        _id: 3,
        firstName: 'Fake first name 3',
        lastName: 'Fake last name 3',
        phone: 'Fake phone 3',
        gender: 'MALE',
      }
    ];
  }
  res.status(status.OK).json({ success: true, data: contacts });
});

/**
 * API: gets the contacts statistics data
 * 
 * @name get/api/contacts/stats
 * @function
 * @inner
 * @param {string} path - Express path
 * @param {callback} middleware - Express middleware.
 */
router.get('/stats', async (req, res) => {
  console.log('Getting the contacts stats');
  let valid = await contactService.count({ deleted: false }).catch(Util.error) || 0;
  let deleted = await contactService.count({ deleted: true }).catch(Util.error) || 0;
  let total = await contactService.count({}).catch(Util.error) || 0;
  let males = await contactService.count({ gender: 'MALE', deleted: false }).catch(Util.error) || 0;
  let females = await contactService.count({ gender: 'FEMALE', deleted: false }).catch(Util.error) || 0;
  const stats = { valid, deleted, males, females, total };
  console.log('Stats loaded from the DB', stats);
  res.status(status.OK).json({ success: true, data: stats });
});

/**
 * API: gets a contact by its ID
 * 
 * @name get/api/contacts/:contactId
 * @function
 * @inner
 * @param {string} path - Express path
 * @param {callback} middleware - Express middleware.
 */
router.get('/:contactId', async (req, res) => {
  const contactId = req.params.contactId;   // Contact's id to find

  console.log(`Getting a contact of id ${contactId}`);

  // Contact's id validation
  if (!contactId || !mongoose.Types.ObjectId.isValid(contactId)) return res.status(status.BAD_REQUEST).json({ success: false, message: 'Bad contact id provided' });
  
  // The query to use, we don't load deleted contacts
  const query = {
    _id: contactId,
    deleted: false
  };
  
  const contactFound = await contactService.findOne(query).catch(Util.error);
  if (contactFound) return res.status(status.OK).json({ success: true, data: contactFound });

  return res.status(status.NOT_FOUND).json({ success: false, message: 'Contact not found' });
});

/**
 * API: deletes a contact by its id
 * 
 * @name delete/api/contacts/:contactId
 * @function
 * @inner
 * @param {string} path - Express path
 * @param {callback} middleware - Express middleware.
 */
router.delete('/:contactId', async (req, res) => {
  const contactId = req.params.contactId;   // Contact's id to delete

  // Contact's id validation
  if (!contactId || !mongoose.Types.ObjectId.isValid(contactId)) return res.status(status.BAD_REQUEST).json({ success: false, message: 'Bad id provided' });

  // Find the contact with id :id
  const contactFound = await contactService.findOne({ _id: contactId }).catch(error => {
    res.status(status.BAD_REQUEST).json({ success: false, message: Util.getErrorMessage(error) });
    return;
  });

  if (contactFound && contactFound._id) {
    // Delete the contact: let's just use a flag
    contactFound.deleted = true;
    const deletedContact = await contactFound.save();
    if (deletedContact) return res.status(status.OK).json({ success: true, message: 'SUCCESS' });
    return res.status(status.BAD_REQUEST).json({ success: false, message: 'An error occured while deleting the contact' });
  } else {
    res.status(status.NOT_FOUND).json({ success: false, message: 'Unknown contact.' });
  }
});

module.exports = router;
