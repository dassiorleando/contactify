# Rezilio - Contactify

Test project following Rezilio job interview, it involves building a contact manager App using React and Bootstrap.

## Dependencies
Let's run `npm install` or `yarn install` to install all the dependencies in the front and server folders.

## Front
The client part of the App is located in the **front** folder.

### Running
Run `yarn start` or `npm start`, the frontend will then be accessible via http://localhost:3000/. Hot reload is enabled so for any updates the UI is refreshed automatically.

## Server
The server part of the App is located in the folder of the same name.

### Running
Run `yarn start` or `npm start` to make the backend accessible at http://localhost:5000/, the contacts CRUD endpoints are located at /api/contacts. Assuming you have  MongoDB installed and running.

### Single Entry Point
To access the App locally from the server URL (http://localhost:5000), we need to define the "PUBLIC_URL" in the front/.env file copied from front/.env.sample, then running the command `yarn run build` or `npm run build` still from the front directory. Assuming the backend is running already.

**Note:** For a production build in lieu of 'http://localhost:5000' we must specify the server path from where the App will be served.

### Resources
- ReactJS: [https://reactjs.org/](https://reactjs.org/)
- Node JS: [https://nodejs.org/en/](https://nodejs.org/en/)
- Bootstrap: [https://getbootstrap.com/](https://getbootstrap.com/)
- Reactstrap: [https://reactstrap.github.io/](https://reactstrap.github.io/)
- ExpressJS: [https://expressjs.com//](https://expressjs.com/)
- Font Awesome 4: [https://fontawesome.com/v4.7.0/examples/](https://fontawesome.com/v4.7.0/examples/)
- TypeScript: [https://www.typescriptlang.org/docs](https://www.typescriptlang.org/docs)
